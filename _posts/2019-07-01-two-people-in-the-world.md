---
layout: post
title:  "There are only two kinds of people in this world… !"
date:   2018-07-01 20:37:13 +0200
categories: general
tags: kinds-of-people
comments: 1
---
Everyone has heard the statement of “There are only two kinds of people in this world….”
I sincerely want the people of my generation to not only live their life to the fullest but to realize that they have the tools within themselves to achieve this. And  I see no better way of living one’s life than to be a producer, not a consumer. The producers of our modern society are our artists, politicians, entrepreneurs, journalists, scientists, and our authors. All of which have literally and figuratively shaped our world through their output and innovation. 

I sincerely want the people of my generation to not only live their life to the fullest but to realize that they have the tools within themselves to achieve something more in their lives. We got the possibility to get the more qualified education, to make some invention, to create useful things for society.

Innovation makes the world better. We got a lot from society. Society has changed the flow of events in the world, which give us a lot of facilities that everyone uses. Every article, every new start up, or every new high tech gadget is produced into to existence, not consumed into existence. These things are produced by someone brave enough to wake up in the morning with a purpose in life.

We can name a lot of names of famous people who did this for future generations. Such people can inspire us to do more than we are doing some now. They are brave enough to wake up in the morning with the intention of creation, not consumption. When an artist paints a masterpiece it will hang in a museum being viewed by countless people for years while no one will ever care to remember what the critics had to say about it.