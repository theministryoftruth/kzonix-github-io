# Alexander Balyshyn

Worked as Software Developer and Software Engineer in projects. For several years worked in the field of e-commerce. Has
rich experience in server-side development, building reactive and scalable systems based on microservice
architecture. My principal interests ate to understand how to design software systems and how to maximize
the productivity and performance of software systems. I am constantly working on understanding the patterns
of good software design, and also the processes that support software design.